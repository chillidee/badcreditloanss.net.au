(function ($) {
  var printMode = false;
  var $calcElement = $('#calc-loan-comparison');
  if (!$calcElement.length) {
    return;
  }

  var result = [];

  function get_inputs() {
    var upfront_fees1 = parseFloat($('#upfront_fees1').val());
    var ongoing_fees_type1 = $('#ongoing_fees_type1').val();
    var ongoing_fees1 = parseFloat($("#ongoing_fees1").val());
    var intro_rate1 = parseFloat($("#intro_rate1").val());
    var intro_term1 = parseInt($("#intro_term1").val());
    var ongoing_rate1 = parseFloat($("#ongoing_rate1").val());

    var upfront_fees2 = parseFloat($('#upfront_fees2').val());
    var ongoing_fees_type2 = $('#ongoing_fees_type2').val();
    var ongoing_fees2 = parseFloat($("#ongoing_fees2").val());
    var intro_rate2 = parseFloat($("#intro_rate2").val());
    var intro_term2 = parseInt($("#intro_term2").val());
    var ongoing_rate2 = parseFloat($("#ongoing_rate2").val());


    var loan_amount = LoanCalc.removeCommas($("#loan_amount").val());
    var loan_term = parseFloat($("#loan_term").val());


    if (isNaN(upfront_fees1))
      upfront_fees1 = 0;

    if (isNaN(ongoing_fees1))
      ongoing_fees1 = 0;

    if (isNaN(intro_rate1))
      intro_rate1 = 0;

    if (isNaN(intro_term1))
      intro_term1 = 0;

    if (isNaN(ongoing_rate1))
      ongoing_rate1 = 0;

    //-----------------------------------

    if (isNaN(upfront_fees2))
      upfront_fees2 = 0;

    if (isNaN(ongoing_fees2))
      ongoing_fees2 = 0;

    if (isNaN(intro_rate2))
      intro_rate2 = 0;

    if (isNaN(intro_term2))
      intro_term2 = 0;

    if (isNaN(ongoing_rate2))
      ongoing_rate2 = 0;

    //-------------------------------

    if (isNaN(loan_amount))
      loan_amount = 0;

    if (isNaN(loan_term))
      loan_term = 0;

    // get the monthly figure

    if (ongoing_fees_type1 == "weekly")
      ongoing_fees1 = ongoing_fees1 * WEEKS_PER_YEAR / MONTHS_PER_YEAR;
    else if (ongoing_fees_type1 == "fortnightly")
      ongoing_fees1 = ongoing_fees1 * FORTNIGHTS_PER_YEAR / MONTHS_PER_YEAR;

    if (ongoing_fees_type2 == "weekly") {
      ongoing_fees2 = ongoing_fees2 * WEEKS_PER_YEAR / MONTHS_PER_YEAR;
    } else if (ongoing_fees_type2 == "fortnightly") {
      ongoing_fees2 = ongoing_fees2 * FORTNIGHTS_PER_YEAR / MONTHS_PER_YEAR;
    }

    return {
      upfront_fees1: upfront_fees1,
      ongoing_fees_type1: ongoing_fees_type1,
      ongoing_fees1: ongoing_fees1,
      intro_rate1: intro_rate1,
      intro_term1: intro_term1,
      ongoing_rate1: ongoing_rate1,

      upfront_fees2: upfront_fees2,
      ongoing_fees_type2: ongoing_fees_type2,
      ongoing_fees2: ongoing_fees2,
      intro_rate2: intro_rate2,
      intro_term2: intro_term2,
      ongoing_rate2: ongoing_rate2,

      loan_amount: loan_amount,
      loan_term: loan_term
    };
  }

  $("#loan_amount_slider").slider({ min: 0, max: 10000000, step: 140000, range: "min", animate: "true", value: 100000,
    slide: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
    },
    stop: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
      redraw();
    }
  });

  $("#loan_term_slider").slider({ min: 0, max: 30, step: 0.5, range: "min", animate: "true", value: 30.0,
    slide: function (event, ui) {
      $("#loan_term").val(ui.value);
    },
    stop: function (event, ui) {
      $("#loan_term").val(ui.value);
      redraw();
    }
  });

  $("#intro_rate_slider1").slider({ min: 0, max: 30.0, step: 0.25, range: "min", animate: "true", value: 6.58,
    slide: function (event, ui) {
      $("#intro_rate1").val(ui.value);
    },
    stop: function (event, ui) {
      $("#intro_rate1").val(ui.value);
      redraw();
    }
  });

  $("#intro_rate_slider2").slider({ min: 0, max: 30.0, step: 0.25, range: "min", animate: "true", value: 6.58,
    slide: function (event, ui) {
      $("#intro_rate2").val(ui.value);
    },
    stop: function (event, ui) {
      $("#intro_rate2").val(ui.value);
      redraw();
    }
  });

  $("#ongoing_rate_slider1").slider({ min: 0, max: 30.0, step: 0.25, range: "min", animate: "true", value: 7.25,
    slide: function (event, ui) {
      $("#ongoing_rate1").val(ui.value);
    },
    stop: function (event, ui) {
      $("#ongoing_rate1").val(ui.value);
      redraw();
    }
  });

  $("#ongoing_rate_slider2").slider({ min: 0.00, max: 30.0, step: 0.25, range: "min", animate: "true", value: 6.75,
    slide: function (event, ui) {
      $("#ongoing_rate2").val(ui.value);
    },
    stop: function (event, ui) {
      $("#ongoing_rate2").val(ui.value);
      redraw();
    }
  });

  $('#intro_term_slider1').slider({min: 0, max: 360, step: 4, range: "min", animate: "true", value: 24,
    slide: function (event, ui) {
      $("#intro_term1").val(ui.value);
    },
    stop: function (event, ui) {
      $("#intro_term1").val(ui.value);
      redraw();
    }
  });

  $('#intro_term_slider2').slider({min: 0, max: 360, step: 4, range: "min", animate: "true", value: 24,
    slide: function (event, ui) {
      $("#intro_term2").val(ui.value);
    },
    stop: function (event, ui) {
      $("#intro_term2").val(ui.value);
      redraw();
    }
  });

  $("#loan_amount").change(function () {
    redraw();
    var value = $(this).val();
    $(this).val(LoanCalc.addCommas(value));
    $("#loan_amount_slider").slider({ 
      value: parseFloat(LoanCalc.removeCommas(value))
    });
  });

  $("#loan_term").change(function () {
    redraw();
    var val = $(this).val();
    $("#loan_term_slider").slider({ 
      value: parseFloat(val)
    });
  });

  $("#upfront_fees1").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#ongoing_fees_type1").change(function () {
    redraw();
  });

  $("#ongoing_fees1").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#intro_rate1").change(function () {
    redraw();
    var val = $(this).val();
    $("#intro_rate_slider1").slider({
      value: parseFloat(val)
    });
  });

  $("#intro_term1").change(function () {
    redraw();
    var val = $(this).val();
    $("#intro_term_slider1").slider({
      value: parseFloat(val)
    });
  });

  $("#ongoing_rate1").change(function () {
    redraw();
    var val = $(this).val();
    $("#ongoing_rate_slider1").slider({
      value: parseFloat(val)
    });
  });

  $("#upfront_fees2").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#ongoing_fees_type2").change(function () {
    redraw();
  });

  $("#ongoing_fees2").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#intro_rate2").change(function () {
    redraw();
    var val = $(this).val();
    $("#intro_rate_slider2").slider({
      value: parseFloat(val)
    });
  });

  $("#intro_term2").change(function () {
    redraw();
    var val = $(this).val();
    $("#intro_term_slider2").slider({
      value: parseFloat(val)
    });
  });

  $("#ongoing_rate2").change(function () {
    redraw();
    var val = $(this).val();
    $("#ongoing_rate_slider2").slider({
      value: parseFloat(val)
    });
  });

  function redraw() {
    var inputs = get_inputs();

    
    var intro_rate1 = inputs.intro_rate1 / 100;
    intro_rate1 /= MONTHS_PER_YEAR;
    var pmt_intro1 = LoanCalc.PMT(intro_rate1, inputs.loan_term * MONTHS_PER_YEAR, inputs.loan_amount, 0);

    var t1 = LoanCalc.total(intro_rate1, inputs.intro_term1, pmt_intro1, inputs.loan_amount);

    var ongoing_rate1 = inputs.ongoing_rate1 / 100;
    ongoing_rate1 /= MONTHS_PER_YEAR;

    var pmt_ongoing1 = LoanCalc.PMT(ongoing_rate1, (inputs.loan_term * MONTHS_PER_YEAR) - inputs.intro_term1, inputs.loan_amount - t1.total_principal, 0);
    pmt_intro1 += inputs.ongoing_fees1;
    t1 = LoanCalc.total(intro_rate1, inputs.intro_term1, pmt_intro1, inputs.loan_amount);
    pmt_ongoing1 += inputs.ongoing_fees1;

    if (!isFinite(pmt_ongoing1)) {
      pmt_ongoing1 = 0;
    }

    $("#res2").html("$" + LoanCalc.addCommas(pmt_intro1.toFixed(0)));
    $("#res4").html("$" + LoanCalc.addCommas(pmt_ongoing1.toFixed(0)));

    
    var q1 = LoanCalc.total(ongoing_rate1, (inputs.loan_term * MONTHS_PER_YEAR) - inputs.intro_term1, pmt_ongoing1, inputs.loan_amount - t1.total_principal);
    var total1 = t1.total_principal + t1.total_interest + q1.total_principal + q1.total_interest + inputs.upfront_fees1;

    $("#res6").html("$" + LoanCalc.addCommas(total1.toFixed(0)));

    //--------------------------------------------------------

    var intro_rate2 = inputs.intro_rate2 / 100;
    intro_rate2 /= MONTHS_PER_YEAR;
    var pmt_intro2 = LoanCalc.PMT(intro_rate2, inputs.loan_term * MONTHS_PER_YEAR, inputs.loan_amount, 0);
    var t2 = LoanCalc.total(intro_rate2, inputs.intro_term2, pmt_intro2, inputs.loan_amount);

    var ongoing_rate2 = inputs.ongoing_rate2 / 100;
    ongoing_rate2 /= MONTHS_PER_YEAR;
    var pmt_ongoing2 = LoanCalc.PMT(ongoing_rate2, (inputs.loan_term * MONTHS_PER_YEAR) - inputs.intro_term2, inputs.loan_amount - t2.total_principal, 0);

    pmt_intro2 += inputs.ongoing_fees2;
    t2 = LoanCalc.total(intro_rate2, inputs.intro_term2, pmt_intro2, inputs.loan_amount);		// recalculate
    pmt_ongoing2 += inputs.ongoing_fees2;

    if (!isFinite(pmt_ongoing2)) {
      pmt_ongoing2 = 0;
    }

    $("#res3").html("$" + LoanCalc.addCommas(pmt_intro2.toFixed(0)));
    $("#res5").html("$" + LoanCalc.addCommas(pmt_ongoing2.toFixed(0)));

    var q2 = LoanCalc.total(ongoing_rate2, (inputs.loan_term * MONTHS_PER_YEAR) - inputs.intro_term2, pmt_ongoing2, inputs.loan_amount - t2.total_principal);
    var total2 = t2.total_principal + t2.total_interest + q2.total_principal + q2.total_interest + inputs.upfront_fees2;

    $("#res7").html("$" + LoanCalc.addCommas(total2.toFixed(0)));

    var x = total1 - total2;
    var y = total2 - total1;
    var title = '';
    var saveTotal = '';

    if (x > y) {
      title = 'Loan #2 will save you:';
      $("#res1").text("$" + (saveTotal = LoanCalc.addCommas(x.toFixed(0))) );
    } else if (x < y) {
      title = 'Loan #1 will save you:';
      $("#res1").text("$" + (saveTotal = LoanCalc.addCommas(y.toFixed(0))) );
    } else {
      title = 'Loan #1 and loan #2 are same.';
      $("#res1").text('').hide();
    }

    $('#res1-title').text(title);

    var arr = new Array();
    var saving = 0;
    var perMonths = (total1 - total2) / inputs.loan_term;

    for (var i = 0; i < inputs.loan_term; i++) {
      saving += perMonths;
      arr[i] = {
        saving: saving
      };
    }

    (function (arr, scale) {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'years');
      data.addColumn('number', 'Savings');

      for (var i = 0; i < arr.length; i++) {
        //if (arr[i].saving < 0) {
        //  arr[i].saving = 0;
        //}
        data.addRow([i / scale, arr[i].saving]);
      }

      var options = {        
        backgroundColor: '#f0efef',
        colors: ['#badfef', 'red'],
        color: '#343739',
        fontSize: '14',
        fontName: 'ff-tisa-web-pro',
        hAxis: {title: 'Years', titleTextStyle: {color: '#343739', italic: '0'}, gridlines: {color: '#858788'}, baseline: {color: '#858788'}, textStyle: {color: '#343739'}},
        vAxis: {title: 'Savings', format: '$#', titleTextStyle: {color: '#343739', italic: '0'}, gridlines: {color: '#858788'}, baseline: {color: '#858788'}, textStyle: {color: '#343739'}},
        legend: {position: 'none'},
        crosshair: {color: '#fff'}     
      };

      var chartElement = document.getElementById('chart_div');

      if (printMode) {
        if ($(chartElement).attr('style')) {
          $(chartElement).removeAttr('style');
        }
        printMode = false;
      }

      var chart = new google.visualization.AreaChart(chartElement);
      chart.draw(data, options);
    }(arr, MONTHS_PER_YEAR))

    result = [
      {key: "Loan Comparison", value: ""},

      {key: "Loan 1", value: ""},
      {key: "upfront fees", value: "$" + inputs.upfront_fees1},
      {key: "ongoing fees frequency", value: inputs.ongoing_fees_type1 },
      {key: "ongoing fees", value: inputs.ongoing_fees1 },
      {key: "intro rate", value: inputs.intro_rate1 + "%"},
      {key: "intro term", value: inputs.intro_term1 + " months"},
      {key: "ongoing rate", value: inputs.ongoing_rate1 + "%"},
      {key: "SEPARATOR"},
      {key: "Loan 2", value: ""},
      {key: "upfront fees", value: "$" + inputs.upfront_fees2},
      {key: "ongoing fees frequency", value: inputs.ongoing_fees_type2 },
      {key: "ongoing fees", value: inputs.ongoing_fees2 },
      {key: "intro rate", value: inputs.intro_rate2 + "%"},
      {key: "intro term", value: inputs.intro_term2 + " months"},
      {key: "ongoing rate", value: inputs.ongoing_rate2 + "%"},
      {key: "SEPARATOR"},
      {key: "loan amount", value: "$" + inputs.loan_amount},
      {key: "loan term", value: inputs.loan_term + " years"},
      {key: "SEPARATOR"},
      {key: "Loan 1", value: ""},
      {key: "initial per month", value: "$" + LoanCalc.addCommas(pmt_intro1.toFixed(0)) },
      {key: "outgoing per month", value: "$" + LoanCalc.addCommas(pmt_ongoing1.toFixed(0)) },
      {key: "total payable", value: "$" + LoanCalc.addCommas(total1.toFixed(0)) },
      {key: "SEPARATOR"},
      {key: "Loan 2", value: ""},
      {key: "initial per month", value: "$" + LoanCalc.addCommas(pmt_intro2.toFixed(0)) },
      {key: "outgoing per month", value: "$" + LoanCalc.addCommas(pmt_ongoing2.toFixed(0)) },
      {key: "total payable", value: "$" + LoanCalc.addCommas(total2.toFixed(0)) },
      {key: "SEPARATOR"},
      {key: title, value: saveTotal !== ''  ? "$" + saveTotal : '' }
    ];
  }

  var printModeReset;
  $(".calc .print").click(function () {
    $('#chart_div').width(400);
    $(window).trigger('resize.chart');
    setTimeout(function () {
      window.print();
      printMode = true;
      clearTimeout(printModeReset);
      printModeReset = setTimeout(function () {
        $(window).trigger('resize.chart');
      }, 1);
    }, 100);
  });

  $(".calc .save").click(function (e) {
    e.preventDefault();
    var data = encodeURIComponent(JSON.stringify(result));
    var win=window.open('save.php?data=' + data + '&title=Loan Comparison', '_blank');
    win.focus();
  });

  $(document).on('click', '.calc .email', function (e) {
    e.preventDefault();
    var $form = $('#email-form');
    var isVisible = $form.is(':visible');

    $form[isVisible ? 'hide' : 'show']();
    $(".calc #message").val(); //.val(isVisible ? '' : LoanCalc.resultToString(result));
  });

  var resizeTimer;
  $(window).on('resize.chart', function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(redraw, 100);
  });

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  redraw();
})(jQuery);



