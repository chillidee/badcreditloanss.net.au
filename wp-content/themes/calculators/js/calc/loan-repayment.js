(function ($) {
  var $calcElement = $('#calc-loan-repayment');
  var printMode = false;

  if (!$calcElement.length) {
    return;
  }

  var result = [];

  function get_inputs() {
    var loan_amount = LoanCalc.removeCommas($("#loan_amount").val());
    var loan_term = parseFloat($("#loan_term").val());
    var interest_rate = parseFloat($("#interest_rate").val());
    var repayment_frequency = $("#repayment_frequency").val();
    var repayment_type = $("#repayment_type").val();

    if (isNaN(loan_amount))
      loan_amount = 0;

    if (isNaN(loan_term))
      loan_term = 0;

    if (isNaN(interest_rate))
      interest_rate = 0;

    return {loan_amount: loan_amount, loan_term: loan_term, interest_rate: interest_rate, repayment_frequency: repayment_frequency, repayment_type: repayment_type};
  }

  $("#loan_amount_slider").slider({ min: 0, max: 5000000, step: 50000, range: "min", animate: "true", value: 100000,
    slide: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
    },
    stop: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
      redraw();
    }
  });

  $("#interest_rate_slider").slider({ min: 0.25, max: 25.0, step: 0.25, range: "min", animate: "true", value: 6.58,
    slide: function (event, ui) {
      $("#interest_rate").val(ui.value);
    },
    stop: function (event, ui) {
      $("#interest_rate").val(ui.value);
      redraw();
    }
  });

  $("#loan_term_slider").slider({ min: 0, max: 30, step: 0.5, range: "min", animate: "true", value: 30,
    slide: function (event, ui) {
      $("#loan_term").val(ui.value);
    },
    stop: function (event, ui) {
      $("#loan_term").val(ui.value);
      redraw();
    }
  });

  $("#loan_amount").change(function () {
    redraw();
    var value = $("#loan_amount").val();
    $(this).val(LoanCalc.addCommas(value));
    $("#loan_amount_slider").slider({ value: value });
  });

  $("#interest_rate").change(function () {
    redraw();
    var v = parseFloat($("#interest_rate").val());
    $("#interest_rate_slider").slider({ value: v });
  });

  $("#loan_term").change(function () {

    redraw();
    var v = parseFloat($("#loan_term").val());
    $("#loan_term_slider").slider({ value: v });
  });

  $("#repayment_frequency").change(function () {
    redraw();
  });

  $("#repayment_type").change(function () {
    redraw();
  });

  function redraw() {
    var inputs = get_inputs();
    var interest = inputs.interest_rate / 100;
    var scale = 0;
    var n;

    if (inputs.repayment_frequency == "weekly") {
      interest = interest / WEEKS_PER_YEAR;
      n = inputs.loan_term * WEEKS_PER_YEAR;
      $("#label1").html("Weekly repayments:");
      scale = WEEKS_PER_YEAR;
    } else {
      if (inputs.repayment_frequency == "fortnightly") {
        interest = interest / FORTNIGHTS_PER_YEAR;
        n = inputs.loan_term * FORTNIGHTS_PER_YEAR;
        $("#label1").html("Fortnightly repayments:");
        scale = FORTNIGHTS_PER_YEAR;
      } else {
        interest = interest / MONTHS_PER_YEAR;
        n = inputs.loan_term * MONTHS_PER_YEAR;
        $("#label1").html("Monthly repayments:");
        scale = MONTHS_PER_YEAR;
      }
    }

    var res = LoanCalc.calculate(inputs.loan_amount, interest, n, inputs.repayment_type);
    $("#res1").html("$" + LoanCalc.addCommas(res.repayment.toFixed(2)));
    var t = res.repayment;
    $("#res2").html("$" + LoanCalc.addCommas(res.total_interest.toFixed(2)));

    var arr = [];
    var p = inputs.loan_amount;
    var a = p + res.total_interest;

    for (var i = 0; i <= n; i++) {
      if (inputs.repayment_type == INTEREST_ONLY) {
        arr[i] = {principal: p, total: a};
        a -= t;
      } else {
        arr[i] = LoanCalc.calculatePeriodRepayment(t, interest, n, i);
        arr[i].principal = p;
        p -= arr[i].principal_repayment;
        arr[i].total = a;
        a -= t;
      }
    }

    (function drawChart(arr,scale) {
      var data = new google.visualization.DataTable();

      data.addColumn('number', 'years');
      data.addColumn('number', 'principal');
      data.addColumn('number', 'total');

      for (var i = 0; i < arr.length; i++)
      {
        if (arr[i].principal < 0)
          arr[i].principal = 0;

        if (arr[i].total < 0)
          arr[i].total = 0;

        data.addRow([i/scale, arr[i].principal/1000, arr[i].total/1000]);
      }

      var options = {
        //title: 'Amount Owing',
        //backgroundColor: "#eee",
        colors: ['#52c7e1','#E4E4E4'],
        //colors:['#A2C180','#3D7930','#FFC6A5','#FFFF42','#DEF3BD','#00A5C6','#DEBDDE','#000000'],
        hAxis: {title: 'Years'},
        //vAxis: {title: 'Amount Owing', format:'$#K', minValue: 0,, titleTextStyle: { fontSize: 12 }},
        vAxis: {title: 'Amount Owing', format:'$#K'},
        legend: {position: 'bottom'},
        //animation: { duration: 1000, easing: 'out', }
      };

      var chartElement = document.getElementById('chart_div');

      if (printMode) {
        if ($(chartElement).attr('style')) {
          $(chartElement).removeAttr('style');
        }
        printMode = false;
      }

      var chart = new google.visualization.AreaChart(chartElement);
      chart.draw(data, options);
    }(arr, scale));

    result = [
      {key: "Loan Repayment", value: ""},
      {key: "loan amount", value: "$" + LoanCalc.addCommas(inputs.loan_amount.toFixed(2))},
      {key: "interest rate", value: inputs.interest_rate + "%"},
      {key: "loan term", value: inputs.loan_term + " years"},
      {key: "repayment frequency", value: inputs.repayment_frequency},
      {key: "repayment type", value: inputs.repayment_type},
      {key: "payments", value: "$" + LoanCalc.addCommas(res.repayment.toFixed(2))},
      {key: "total interest", value: "$" + LoanCalc.addCommas(res.total_interest.toFixed(2))}
    ];
  }

  var printModeReset;
  $(".calc .print").click(function () {
    $('#chart_div').width(400);
    $(window).trigger('resize.chart');
    setTimeout(function () {
      window.print();
      printMode = true;
      clearTimeout(printModeReset);
      printModeReset = setTimeout(function () {
        $(window).trigger('resize.chart');
      }, 1);
    }, 100);
  });

  $(".calc .save").click(function (e) {
    e.preventDefault();
    var data = encodeURIComponent(JSON.stringify(result));
    var win=window.open('save.php?data=' + data + '&title=Loan Repayment', '_blank');
    win.focus();
  });

  $(document).on('click', '.calc .email', function (e) {
    e.preventDefault();
    var $form = $('#email-form');
    var isVisible = $form.is(':visible');

    $form[isVisible ? 'hide' : 'show']();
    $(".calc #message").val(); //.val(isVisible ? '' : LoanCalc.resultToString(result));
  });

  var resizeTimer;
  $(window).on('resize.chart', function () {
    console.log('resize detected');
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(redraw, 100);
  });

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  redraw();
})(jQuery);



