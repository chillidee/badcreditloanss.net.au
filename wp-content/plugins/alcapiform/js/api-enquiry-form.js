jQuery.noConflict()(function ($) {
    $(document).ready(function () {

        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;

        $(".next0").click(function () {

            var myloanAmount = document.apiEnquiryForm.loanAmount.value;
            myloanAmount = myloanAmount.replace("$", "");
            myloanAmount = myloanAmount.replace(",", "");
            myloanAmount = Number(myloanAmount)

            if (document.apiEnquiryForm.loanAmount.value == "" || document.apiEnquiryForm.hasProperty.value == "") {
                document.getElementById("wrongmessage").innerHTML = "Please Fill Your Information to continue";
                errorNotification();
            } else if (isUnsecuredDebts() && !jQuery("#bankruptyes").is(":checked") && !jQuery("#bankruptno").is(":checked")) {
                document.getElementById("wrongmessage").innerHTML = "Please Fill Your Information to continue";
                errorNotification();
            } else if (isUnsecuredDebts() && jQuery("#bankruptno").is(":checked")) {
                current_step = $(this).parent();
                next_step = $(this).parent().next().next().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage").innerHTML = "";
            } else if (isPersonalQuestions()) {
                current_step = $(this).parent();
                next_step = $(this).parent().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage").innerHTML = "";
                if (!isUnsecuredDebts())
                    jQuery("#bankruptSection").remove();
            }
            else {
                current_step = $(this).parent();
                next_step = $(this).parent().next().next().next().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage").innerHTML = "";
                if (!isUnsecuredDebts())
                    jQuery("#bankruptSection").remove();
            }
        });

        $(".next1").click(function () {
            if (document.apiEnquiryForm.firstName.value == "" || document.apiEnquiryForm.mobileNumber.value == "" || document.apiEnquiryForm.emailAddress.value == "") {
                document.getElementById("wrongmessage1").innerHTML = "Please Fill Your Information to continue";
                errorNotification();
            }
            else {
                current_step = $(this).parent();
                next_step = $(this).parent().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage1").innerHTML = "";
            }
        });

        $(".next2").click(function () {
            if (document.apiEnquiryForm.hasAuvisa.value == "" || document.apiEnquiryForm.bankrupt.value == "" || document.apiEnquiryForm.currentempl.value == "" || (document.apiEnquiryForm.bankrupt.value == "1" && document.apiEnquiryForm.discharged.value == "") || (document.apiEnquiryForm.currentempl.value == "1" && document.apiEnquiryForm.currentjob2yr.value == "")) {
                document.getElementById("wrongmessage2").innerHTML = "Please Fill Your Information to continue";
                errorNotification();
            }

            else {
                current_step = $(this).parent();
                next_step = $(this).parent().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage2").innerHTML = "";
            }
        });

        $(".next3").click(function () {
            if (document.apiEnquiryForm.recentjudge.value == "" || document.apiEnquiryForm.currentaddr.value == "" || document.apiEnquiryForm.audrivelic.value == "" || (document.apiEnquiryForm.recentjudge.value == "1" && document.apiEnquiryForm.bepaid.value == "") || (document.apiEnquiryForm.bepaid.value == "1" && document.apiEnquiryForm.judgetime.value == "")) {
                document.getElementById("wrongmessage3").innerHTML = "Please Fill Your Information to continue";
                errorNotification();
            }

            else {
                current_step = $(this).parent();
                next_step = $(this).parent().next()
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage3").innerHTML = "";
            }
        });

        $(".next4").click(function () {
            if ($("#unsecuredDebtList > div").length == 0 && $('#unsecuredDebtFields select').val() != 'I have no debts') {
                document.getElementById("wrongmessage4").innerHTML = "Please Add Unsecured Debt to continue";
                errorNotification();
            }

            else {
                current_step = $(this).parent();
                next_step = $(this).parent().next()
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
                document.getElementById("wrongmessage4").innerHTML = "";
            }
        });

        $(".previous").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });

        $(".previous4").click(function () {
            current_step = $(this).parent();
            if (isPersonalQuestions()) {
                next_step = $(this).parent().prev();
                next_step.show();
            } else {
                next_step = $(this).parent().prev().prev().prev();
                next_step.show();
            }
            current_step.hide();
            setProgressBar(--current);
        });

        $(".previous1").click(function () {
            var myloanAmount = document.apiEnquiryForm.loanAmount.value;
            myloanAmount = myloanAmount.replace("$", "");
            myloanAmount = myloanAmount.replace(",", "");
            myloanAmount = Number(myloanAmount)

            if ((isUnsecuredDebts() && jQuery("#bankruptno").is(":checked")) || isPersonalQuestions()) {
                current_step = $(this).parent();
                next_step = $(this).parent().prev();
                next_step.show();
                current_step.hide();
                setProgressBar(--current);
            }
            else {
                current_step = $(this).parent();
                next_step = $(this).parent().prev().prev().prev().prev();
                next_step.show();
                current_step.hide();
                setProgressBar(--current);
            }
        });

        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width", percent + "%")
                .html(percent + "%");
        }


        $(".fieldset-bankrup #bankruptyes").click(function () {
            document.getElementById("discharged-block").style.display = "block";
        });

        $(".fieldset-bankrup #bankruptno").click(function () {
            document.getElementById("dischargedyes").checked = false;
            document.getElementById("dischargedno").checked = false;
            document.getElementById("discharged-block").style.display = "none";
        });

        $("#currentemplyes").click(function () {
            document.getElementById("time-job-block").style.display = "block";
        });

        $("#currentemplno").click(function () {
            document.getElementById("currentjob2yrmore").checked = false;
            document.getElementById("currentjob2yrless").checked = false;
            document.getElementById("time-job-block").style.display = "none";
        });

        $("#recentjudgeyes").click(function () {
            document.getElementById("paid-block").style.display = "block";
            document.getElementById("judgetime-block").style.display = "block";
        });

        $("#recentjudgeno").click(function () {
            document.getElementById("paidno").checked = false;
            document.getElementById("paidyes").checked = false;
            document.getElementById("paid-block").style.display = "none";
            document.getElementById("judgetime2yrmore").checked = false;
            document.getElementById("judgetime2yrless").checked = false;
            document.getElementById("judgetime-block").style.display = "none";
        });

        $("#paidyes").click(function () {
            document.getElementById("judgetime-block").style.display = "block";
        });

        $("#paidno").click(function () {
            document.getElementById("judgetime2yrmore").checked = false;
            document.getElementById("judgetime2yrless").checked = false;
            document.getElementById("judgetime-block").style.display = "none";
        });

        function toggleBankruptSection() {
            if (isUnsecuredDebts()) {
                jQuery("#bankruptSection").show();
            } else {
                jQuery("#bankruptSection").hide();
                if (!isPersonalQuestions()) {
                    jQuery("#bankruptyes, #bankruptno").prop("checked", false);
                }
            }
        }

        function isPersonalQuestions() {
            var hasNoProperty = jQuery("#hasPropertyNo").is(":checked");

            var myloanAmount = document.apiEnquiryForm.loanAmount.value;
            myloanAmount = Number(myloanAmount.replace("$", "").replace(",", ""));

            var mytypeOfLoan = jQuery("#typeOfLoan").val();

            return (hasNoProperty && myloanAmount > 2500 && myloanAmount <= 50000 && (mytypeOfLoan == "2" || mytypeOfLoan == "17" || mytypeOfLoan == "135"));
        }

        function isUnsecuredDebts() {
            var hasNoProperty = jQuery("#hasPropertyNo").is(":checked");

            var myloanAmount = document.apiEnquiryForm.loanAmount.value;
            myloanAmount = Number(myloanAmount.replace("$", "").replace(",", ""));

            var mytypeOfLoan = jQuery("#typeOfLoan").val();

            return (hasNoProperty && myloanAmount <= 2500 && (mytypeOfLoan == "126" || mytypeOfLoan == "17" || mytypeOfLoan == "135"));
        }

        var web_url = document.getElementById("website_url");
        web_url.parentNode.removeChild(web_url);

        var ERROR_1 = "<li>Please tell us if you currently own or paying off real estate.</li>";
        var ERROR_2 = "<li>Please choose a type of loan you're interested in.</li>";
        var ERROR_3 = "<li>Loan amount is required.</li>";
        var ERROR_4 = "<li>Total real estate value required.</li>";
        var ERROR_5 = "<li>Balance owing required.</li>";
        var ERROR_6 = "<li>Mobile is required.</li>";
        var ERROR_7 = "<li>Mobile number is incomplete, or not in the correct format.</li>";
        var ERROR_8 = "<li>Email is required.</li>";
        var ERROR_9 = "<li>Invalid email format.</li>";
        var ERROR_10 = "<li>Your PostCode is required.</li>";
        var ERROR_11 = "<li>Postcode is incomplete or invalid.</li>";
        var ERROR_12 = "<li>First name is required.</li>";
        var ERROR_13 = "<li>Last name is required.</li>";
        var ERROR_14 = "<li>Landline number is incomplete, or not in the correct format.</li>";
        var ERROR_15 = "<li>Suburb is required.</li>";
        var ERROR_16 = "<li>Please choose where did you hear about us?</li>";
        var ERROR_17 = "<li>Please tell us if you have a minimum 20% deposit of the purchase price.</li>";
        var ERROR_18 = "<li>Type is required.</li>";
        var ERROR_19 = "<li>Please accept the privacy policy.</li>";
        var ERROR_20 = "<li>Credit Limit is required and must be greater than 0.</li>";
        var ERROR_21 = "<li>Amount Owing is required and must be greater than 0.</li>";
        var ERROR_22 = "<li>Provider/Owed To is required.</li>";

        var error_message = "";

        jQuery("#loanAmount, #realEstateValue, #balanceOwing, #unsecuredDebt_CreditLimit, #unsecuredDebt_Amount").blur(function () {
            toggleBankruptSection();
            var value = jQuery(this).val().replace(/[^\d\.]/g, "");
            value = value.trim();
            if (value == "") {
                jQuery(this).val("");
            } else {
                value = Math.round(value);                
                jQuery(this).val(value);

                if (jQuery(this).attr("id") == "loanAmount") {
                    jQuery("#loanAmountError").hide();
                    error_message = error_message.replace(ERROR_3, "");
                }

                if (jQuery(this).attr("id") == "realEstateValue") {
                    jQuery("#realEstateValueError").hide();
                    error_message = error_message.replace(ERROR_4, "");
                }

                if (jQuery(this).attr("id") == "balanceOwing") {
                    jQuery("#balanceOwingError").hide();
                    error_message = error_message.replace(ERROR_5, "");
                }

                if (jQuery(this).attr("id") == "unsecuredDebt_CreditLimit") {
                    jQuery("#unsecuredDebt_CreditLimitError").hide();
                    error_message = error_message.replace(ERROR_20, "");
                }

                if (jQuery(this).attr("id") == "unsecuredDebt_Amount") {
                    jQuery("#unsecuredDebt_AmountError").hide();
                    error_message = error_message.replace(ERROR_21, "");
                }

                showErrorMessages();
            }
        });
        jQuery("#loanAmount, #realEstateValue, #balanceOwing, #unsecuredDebt_CreditLimit, #unsecuredDebt_Amount").keypress(function (event) {
            if (event.which < 48 || event.which > 57 || jQuery(this).val().length > 9) {
                event.preventDefault();
            }
        });
        jQuery("#loanAmount").keyup(function (event) {
            toggleBankruptSection();
        });
        jQuery("#loanAmount, #realEstateValue, #balanceOwing, #unsecuredDebt_CreditLimit, #unsecuredDebt_Amount").focusin(function () {
            jQuery(this).val(jQuery(this).val().replace(/[$,]/g, ""));
        });


        jQuery("#mobileNumber").focusin(function () {
            jQuery("#mobileNumberError").hide();
            error_message = error_message.replace(ERROR_6, "");
            error_message = error_message.replace(ERROR_7, "");
            showErrorMessages();
        });
        jQuery("#mobileNumber").blur(function () {
            var value = jQuery(this).val().replace(/[^\d]/g, "");
            value = value.replace(/^61/, "0");
            jQuery(this).val(value);
            check_mobileNumber();
            showErrorMessages();
        });


        jQuery("#emailAddress").focusin(function () {
            jQuery("#emailAddressError").hide();
            error_message = error_message.replace(ERROR_8, "");
            error_message = error_message.replace(ERROR_9, "");
            showErrorMessages();
        });
        jQuery("#emailAddress").blur(function () {
            check_emailAddress();
            showErrorMessages();
        });


        jQuery("#landLineNumber").focusin(function () {
            jQuery("#landLineNumberError").hide();
            error_message = error_message.replace(ERROR_14, "");
            showErrorMessages();
        });
        jQuery("#landLineNumber").blur(function () {
            var value = jQuery(this).val().replace(/[^\d]/g, "");
            jQuery(this).val(value);
            check_landLineNumber();
            showErrorMessages();
        });


        jQuery("#postCode").focusin(function () {
            jQuery("#postCodeError").hide();
            error_message = error_message.replace(ERROR_10, "");
            error_message = error_message.replace(ERROR_11, "");
            showErrorMessages();
        });
        jQuery("#postCode").blur(function () {
            var value = jQuery(this).val().replace(/[^\d]/g, "");
            jQuery(this).val(value);
            check_postCode();
            showErrorMessages();
        });

        jQuery("#firstName").focusin(function () {
            jQuery("#firstNameError").hide();
            error_message = error_message.replace(ERROR_12, "");
            showErrorMessages();
        });
        jQuery("#firstName").blur(function () {
            var value = jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
            jQuery(this).val(value);
            check_firstName();
            showErrorMessages();
        });

        jQuery("#lastName").focusin(function () {
            jQuery("#lastNameError").hide();
            error_message = error_message.replace(ERROR_13, "");
            showErrorMessages();
        });
        jQuery("#lastName").blur(function () {
            var value = jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
            jQuery(this).val(value);
            check_lastName();
            showErrorMessages();
        });

        jQuery("#suburb").focusin(function () {
            jQuery("#suburbError").hide();
            error_message = error_message.replace(ERROR_15, "");
            showErrorMessages();
        });
        jQuery("#terms").focusin(function () {
            jQuery("#termsError").hide();
            error_message = error_message.replace(ERROR_18, "");
            showErrorMessages();
        });
        jQuery("#suburb").blur(function () {
            var value = jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
            jQuery(this).val(value);
            check_suburb();
            showErrorMessages();
        });


        jQuery("#typeOfLoan").change(function () {
            check_typeOfLoan();
            showErrorMessages();
            toggleBankruptSection();
        });

        jQuery("#referral").change(function () {
            check_referral();
            showErrorMessages();
        });

        jQuery("input[type=radio][name=hasProperty]").change(function () {
            jQuery("#hasPropertyError").hide();
            error_message = error_message.replace(ERROR_1, "");
            showErrorMessages();
            if (this.value == "1") {
                jQuery("#yourPropertySection").show();
            } else {
                jQuery("#yourPropertySection").hide();
            }
            toggleBankruptSection();
        });

        jQuery("input[type=radio][name=haveDeposit]").change(function () {
            jQuery("#haveDepositError").hide();
            error_message = error_message.replace(ERROR_17, "");
            showErrorMessages();
        });


        jQuery("#apiSubmit").click(function () {

            if (jQuery("#loanAmount").length && jQuery("#loanAmount").val() == "") {
                jQuery("#loanAmountError").html("required");
                jQuery("#loanAmountError").show();
                error_message = error_message.replace(ERROR_3, "");
                error_message += ERROR_3;
            }

            if (!jQuery("#terms").prop("checked")) {
                jQuery("#termsError").html("We would love to help you, but we can't unless you accept our privacy policy.");
                jQuery("#termsError").show();
                jQuery("#termsError").css("display", "block");
                error_message = error_message.replace(ERROR_18, "");
                error_message += ERROR_18;
            }

            if (jQuery("input[name=hasProperty]").length && jQuery("input[name=hasProperty]:checked").length == 0) {
                jQuery("#hasPropertyError").html("required");
                jQuery("#hasPropertyError").show();
                error_message = error_message.replace(ERROR_1, "");
                error_message += ERROR_1;
            }

            if (jQuery("input[name=haveDeposit]").length && jQuery("input[name=haveDeposit]:checked").length == 0) {
                jQuery("#haveDepositError").html("required");
                jQuery("#haveDepositError").show();
                error_message = error_message.replace(ERROR_17, "");
                error_message += ERROR_17;
            }

            if (jQuery("#hasPropertyYes").is(":checked")) {
                if (jQuery("#realEstateValue").val() == "") {
                    jQuery("#realEstateValueError").html("required");
                    jQuery("#realEstateValueError").show();
                    error_message = error_message.replace(ERROR_4, "");
                    error_message += ERROR_4;
                }
                if (jQuery("#balanceOwing").val() == "") {
                    jQuery("#balanceOwingError").html("required");
                    jQuery("#balanceOwingError").show();
                    error_message = error_message.replace(ERROR_5, "");
                    error_message += ERROR_5;
                }
            }


            if (jQuery("#typeOfLoan").length) check_typeOfLoan();
            if (jQuery("#mobileNumber").length) check_mobileNumber();
            if (jQuery("#emailAddress").length) check_emailAddress();
            if (jQuery("#firstName").length) check_firstName();
            if (jQuery("#lastName").length) check_lastName();
            if (jQuery("#suburb").length) check_suburb();
            if (jQuery("#postCode").length) check_postCode();
            if (jQuery("#referral").length) check_referral();

            if (error_message == "") {
                jQuery("#apiSubmit").prop("disabled", true);
                jQuery("#apiEnquiryForm").submit();
            } else {
                showErrorMessages();
            }
        });

        function check_typeOfLoan() {
            if (jQuery("#typeOfLoan").val() == "") {
                jQuery("#typeOfLoanError").html("required");
                jQuery("#typeOfLoanError").show();
                error_message = error_message.replace(ERROR_2, "");
                error_message += ERROR_2;
            } else {
                jQuery("#typeOfLoanError").hide();
                error_message = error_message.replace(ERROR_2, "");
                showErrorMessages();
            }
        }

        function check_referral() {
            if (jQuery("#referral").val() == "") {
                jQuery("#referralError").html("required");
                jQuery("#referralError").show();
                error_message = error_message.replace(ERROR_16, "");
                error_message += ERROR_16;
            } else {
                jQuery("#referralError").hide();
                error_message = error_message.replace(ERROR_16, "");
                showErrorMessages();
            }
        }

        function check_landLineNumber() {
            if (jQuery("#landLineNumber").val() != "" && jQuery("#landLineNumber").val().length != 8) {
                jQuery("#landLineNumberError").html("invalid");
                jQuery("#landLineNumberError").show();
                error_message = error_message.replace(ERROR_14, "");
                error_message += ERROR_14;
            } else {
                jQuery("#landLineNumberError").hide();
                error_message = error_message.replace(ERROR_14, "");
            }
        }

        function check_mobileNumber() {
            if (jQuery("#mobileNumber").val() == "") {
                jQuery("#mobileNumberError").html("required");
                jQuery("#mobileNumberError").show();
                error_message = error_message.replace(ERROR_6, "");
                error_message += ERROR_6;
            } else if (jQuery("#mobileNumber").val().indexOf("04") != 0 || jQuery("#mobileNumber").val().length != 10) {
                jQuery("#mobileNumberError").html("invalid");
                jQuery("#mobileNumberError").show();
                error_message = error_message.replace(ERROR_7, "");
                error_message += ERROR_7;
            } else {
                jQuery("#mobileNumberError").hide();
                error_message = error_message.replace(ERROR_6, "");
                error_message = error_message.replace(ERROR_7, "");
            }
        }

        function check_emailAddress() {
            jQuery("#emailAddress").val(jQuery("#emailAddress").val().replace(" ", ""))
            if (jQuery("#emailAddress").val() == "") {
                jQuery("#emailAddressError").html("required");
                jQuery("#emailAddressError").show();
                error_message = error_message.replace(ERROR_8, "");
                error_message += ERROR_8;
            } else if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(jQuery("#emailAddress").val()) == false) {
                jQuery("#emailAddressError").html("invalid");
                jQuery("#emailAddressError").show();
                error_message = error_message.replace(ERROR_9, "");
                error_message += ERROR_9;
            } else {
                jQuery("#emailAddressError").hide();
                error_message = error_message.replace(ERROR_8, "");
                error_message = error_message.replace(ERROR_9, "");
            }
        }

        function check_firstName() {
            if (jQuery("#firstName").val() == "") {
                jQuery("#firstNameError").html("required");
                jQuery("#firstNameError").show();
                error_message = error_message.replace(ERROR_12, "");
                error_message += ERROR_12;
            } else {
                jQuery("#firstNameError").hide();
                error_message = error_message.replace(ERROR_12, "");
            }
        }

        function check_lastName() {
            if (jQuery("#lastName").val() == "") {
                jQuery("#lastNameError").html("required");
                jQuery("#lastNameError").show();
                error_message = error_message.replace(ERROR_13, "");
                error_message += ERROR_13;
            } else {
                jQuery("#lastNameError").hide();
                error_message = error_message.replace(ERROR_13, "");
            }
        }

        function check_suburb() {
            if (jQuery("#suburb").val() == "") {
                jQuery("#suburbError").html("required");
                jQuery("#suburbError").show();
                error_message = error_message.replace(ERROR_15, "");
                error_message += ERROR_15;
            } else {
                jQuery("#suburbError").hide();
                error_message = error_message.replace(ERROR_15, "");
            }
        }

        function check_postCode() {
            if (jQuery("#postCode").val() == "") {
                jQuery("#postCodeError").html("required");
                jQuery("#postCodeError").show();
                error_message = error_message.replace(ERROR_10, "");
                error_message += ERROR_10;
            } else if (jQuery("#postCode").val().length != 4) {
                jQuery("#postCodeError").html("invalid");
                jQuery("#postCodeError").show();
                error_message = error_message.replace(ERROR_11, "");
                error_message += ERROR_11;
            } else {
                jQuery("#postCodeError").hide();
                error_message = error_message.replace(ERROR_10, "");
                error_message = error_message.replace(ERROR_11, "");
            }
        }

        function showErrorMessages() {
            if (error_message == "") {
                jQuery("#error_message").html("");
            } else {
                jQuery("#error_message").html("<b>Oops, we've missed something...</b><ul>" + error_message + "</ul>");
            }
        }

        if (localStorage.getItem("privacy") === "accept") {
            localStorage.removeItem("privacy");
            jQuery("#apiSubmit").trigger("click");
        }



        var global_item_id = 0;

        $("#addUnsecuredDebt").on("click", function () {

            if (!validadeUnsecuredDebt())
                return;

            // Clone fields with values
            global_item_id += 1;
            var item_id = global_item_id;
            //var item_id = $('#unsecuredDebtList > div').length + 1;
            var item_header = $('#unsecuredDebtFields select option:selected').text();
            var item_body = $('#unsecuredDebtFields').clone().prop('id', 'unsecuredDebtFields_item_' + item_id);
            $(item_body).find('#unsecuredDebt_Type').val(item_header);
            $(item_body).find('label[for="typeOfUnsecuredDebt"]').hide();
            $(item_body).find('#typeOfUnsecuredDebt').hide();
            if (item_header != 'Credit Card or Store Card')
                $(item_body).find('#unsecuredDebtCreditLimitSection').removeClass('unsecuredDebtField'); // It can't be toggled

            // Clear fields to get ready to a new one
            $('#unsecuredDebtFields select').val('');
            $('#unsecuredDebtFields input').val('');
            $('#unsecuredDebtFields #unsecuredDebtCreditLimitSection').hide();

            // Set readonly attribute in cloned fields
            $(item_body).find('input').prop('readonly', true);

            // Add actions edit/remove and events
            var buttonsContainer = '<div class="buttonsUnsecureDebt"></div>';
            var editUnsecuredDebt = '<input type="button" id="editUnsecuredDebt_' + item_id + '" style="width:45px!important;float:left;background-image:url(wp-content/plugins/alcapiform/img/editUnsecuredDebt.png);' +
                'background-repeat:no-repeat;background-position:center;background-size:32px 32px;padding:0 40px!important;margin-right:8px" class="next0 btn btn-info">';
            var removeUnsecuredDebt = '<input type="button" id="removeUnsecuredDebt_' + item_id + '" style="width:45px!important;float:left;background-image:url(wp-content/plugins/alcapiform/img/removeUnsecuredDebt.png);' +
                'background-repeat:no-repeat;background-position:center;background-size:32px 32px;padding:0 40px!important" class="next0 btn btn-info removeUnsecuredDebt">';

            $('#unsecuredDebtList').on("click", '#removeUnsecuredDebt_' + item_id, function () {

                // Remove Unsecured Debt
                $('#unsecuredDebtFields_item_' + item_id).prev().remove();
                $('#unsecuredDebtFields_item_' + item_id).remove();
                unsecuredDebtListAccordion();
            });

            $('#unsecuredDebtList').on("click", '#editUnsecuredDebt_' + item_id, function () {

                // Copy values to main area  	
                $('#unsecuredDebtFields select').val($('#unsecuredDebtFields_item_' + item_id).prev().text());
                $('#unsecuredDebtFields #unsecuredDebt_CreditLimit').val($('#unsecuredDebtFields_item_' + item_id + ' #unsecuredDebt_CreditLimit').val());
                $('#unsecuredDebtFields #unsecuredDebt_Amount').val($('#unsecuredDebtFields_item_' + item_id + ' #unsecuredDebt_Amount').val());
                $('#unsecuredDebt_ProviderOwedTo').val($('#unsecuredDebtFields_item_' + item_id + ' #unsecuredDebt_ProviderOwedTo').val());

                // Small treatments
                $("#addUnsecuredDebt").val('Save Changes');
                $("#unsecuredDebtFields select").change();

                // Remove Unsecured Debt
                $('#removeUnsecuredDebt_' + item_id).click();
            });

            $(item_body).append($(buttonsContainer).append(editUnsecuredDebt).append(removeUnsecuredDebt));

            // Add unsecured debt to the list
            $('#unsecuredDebtList').append('<h3>' + item_header + '</h3>').append(item_body);

            unsecuredDebtListAccordion();

            $("#addUnsecuredDebt").val('Add New/Save');
        });

        function unsecuredDebtListAccordion() {
            // Set up accordion for items
            if (!!$("#unsecuredDebtList").data("ui-accordion")) {
                $('#unsecuredDebtList').accordion("destroy");
            }
            $('#unsecuredDebtList').accordion({ active: false, collapsible: true });
        }

        $("#unsecuredDebtList").bind("DOMSubtreeModified", function () {
            // Sum up amount of unsecured debts
            var unsecuredDebtTotal = 0;
            $("#unsecuredDebtList input[id*='unsecuredDebt_Amount']").each(function () {
                unsecuredDebtTotal += Number($(this).val().replace(/[^\d\.]/g, ""));
            });
            // Update total
            unsecuredDebtTotal = Math.round(unsecuredDebtTotal) + "";
            unsecuredDebtTotal = unsecuredDebtTotal.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('#unsecuredDebtTotal').val("$" + unsecuredDebtTotal);
        });

        function validadeUnsecuredDebt() {

            $('#unsecuredDebtFields #typeOfUnsecuredDebtError').hide();
            $('#unsecuredDebtFields #unsecuredDebt_CreditLimitError').hide();
            $('#unsecuredDebtFields #unsecuredDebt_AmountError').hide();
            $('#unsecuredDebt_ProviderOwedToError').hide();

            if ($('#unsecuredDebtFields select').val() == '') {
                $('#unsecuredDebtFields select').focus();
                $('#unsecuredDebtFields #typeOfUnsecuredDebtError').show();
                return false;
            } else if ($('#unsecuredDebtFields select').val() == 'Credit Card or Store Card' && ($('#unsecuredDebtFields #unsecuredDebt_CreditLimit').val() == '' || Number($('#unsecuredDebtFields #unsecuredDebt_CreditLimit').val().replace(/[^\d\.]/g, "")) == 0)) {
                $('#unsecuredDebtFields #unsecuredDebt_CreditLimit').focus();
                $('#unsecuredDebtFields #unsecuredDebt_CreditLimitError').show();
                return false;
            } else if ($('#unsecuredDebtFields #unsecuredDebt_Amount').val() == '' || Number($('#unsecuredDebtFields #unsecuredDebt_Amount').val().replace(/[^\d\.]/g, "")) == 0) {
                $('#unsecuredDebtFields #unsecuredDebt_Amount').focus();
                $('#unsecuredDebtFields #unsecuredDebt_AmountError').show();
                return false;
            } else if ($('#unsecuredDebtFields #unsecuredDebt_ProviderOwedTo').val() == '') {
                $('#unsecuredDebtFields #unsecuredDebt_ProviderOwedTo').focus();
                $('#unsecuredDebtFields #unsecuredDebt_ProviderOwedToError').show();
                return false;
            }
            return true;
        }

        $("#unsecuredDebtFields select").on("change", function () {
            if ($('#unsecuredDebtFields select').val() == 'Credit Card or Store Card') {
                toggleUnsecuredDebtFields(true);
                $('#unsecuredDebtFields #unsecuredDebtCreditLimitSection').show();
            } else if ($('#unsecuredDebtFields select').val() == 'I have no debts') {
                toggleUnsecuredDebtFields(false);
                cleanUnsecuredDebtList();
            } else {
                toggleUnsecuredDebtFields(true);
                $('#unsecuredDebtFields #unsecuredDebtCreditLimitSection').hide();
            }
        });

        function toggleUnsecuredDebtFields(isVisible) {
            isVisible ? $('.unsecuredDebtField').show() : $('.unsecuredDebtField').hide()
        }

        function cleanUnsecuredDebtList() {
            $("#unsecuredDebtList").empty();
            $('#unsecuredDebtTotal').val('0');
        }

        function errorNotification() {
            $('.wrongmessagewrapper').effect("shake", { times: 2, distance: 20 }, 400);
        }
    });
});